package com.domain;

import com.exception.InvalidInputException;

import java.util.Objects;

public class Employee {
    private final int departmentId;
    private final String name;
    private final String gender;
    private final double salary;
    private final int age;

    public int getAge() {
        return age;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public String getName() {
        return name;
    }

    public String getGender() {
        return gender;
    }

    public double getSalary() {
        return salary;
    }

    private Employee(int departmentId, String name, String gender, double salary, int age) {
        this.departmentId = departmentId;
        this.name = name;
        this.gender = gender;
        this.salary = salary;
        this.age = age;
    }

    public static class Builder {
        private static final String INVALID_AGE = "Invalid age: %s";
        private static final String INVALID_DEPARTMENT = "Invalid department id: %s";
        private static final String INVALID_SALARY = "Invalid salary: %s";

        private static final String EMPLOYEE_DEPARTMENT_ID_CANNOT_BE_NULL_OR_EMPTY =
                "Employee department id cannot be null or empty";
        private static final String EMPLOYEE_NAME_CANNOT_BE_NULL_OR_EMPTY = "Employee name cannot be null or empty";
        private static final String EMPLOYEE_SALARY_CANNOT_BE_NULL_OR_EMPTY = "Employee salary cannot be null or empty";
        private static final String EMPLOYEE_GENDER_CANNOT_BE_NULL_OR_EMPTY = "Employee gender cannot be null or empty";
        private static final String EMPLOYEE_AGE_CANNOT_BE_LESS_THEN_18 = "Employee age cannot be less then 18";

        private int departmentId;
        private String name;
        private String gender;
        private double salary;
        private int age;

        public Builder setDepartmentId(final int departmentId) {
            validateDepartmentId(departmentId);
            this.departmentId = departmentId;
            return this;
        }

        public Builder setAge(final int age) {
            validateAge(age);
            this.age = age;
            return this;
        }

        public Builder setAge(final String age) {
            try {
                this.age = Integer.parseInt(age);
            } catch (NumberFormatException exception) {
                throw new InvalidInputException(String.format(INVALID_AGE, age));
            }
            return this;
        }

        public Builder setDepartmentId(final String departmentId) {
            this.departmentId = validateDepartmentIdString(departmentId);
            return this;
        }

        public Builder setName(final String name) {
            this.name = name;
            return this;
        }

        public Builder setGender(String gender) {
            this.gender = gender;
            return this;
        }

        public Builder setSalary(final double salary) {
            this.salary = salary;
            return this;
        }

        public Builder setSalary(final String salary) {
            validateSalaryStringIsValid(salary);
            return this;
        }

        public Employee build() {
            performValidations();
            return new Employee(departmentId, name, gender, salary, age);
        }

        private void performValidations() {
            validateName(name);
            validateSalary(salary);
            validateGender(gender);
            validateAge(age);
            validateDepartmentId(departmentId);
        }

        private void validateName(String name) {
            if (isNullOrEmpty(name)) {
                throw new InvalidInputException(EMPLOYEE_NAME_CANNOT_BE_NULL_OR_EMPTY);
            }
        }

        private void validateSalary(double salary) {
            if (salary < 0) {
                throw new InvalidInputException(EMPLOYEE_SALARY_CANNOT_BE_NULL_OR_EMPTY);
            }
        }

        private void validateGender(String gender) {
            if (isNullOrEmpty(gender)) {
                throw new InvalidInputException(EMPLOYEE_GENDER_CANNOT_BE_NULL_OR_EMPTY);
            }
        }

        private void validateAge(int age) {
            if (age < 18) {
                throw new InvalidInputException(EMPLOYEE_AGE_CANNOT_BE_LESS_THEN_18);
            }
        }

        private void validateDepartmentId(int departmentId) {
            if (Objects.isNull(departmentId)) {
                throw new InvalidInputException(EMPLOYEE_DEPARTMENT_ID_CANNOT_BE_NULL_OR_EMPTY);
            }
        }

        private void validateSalaryStringIsValid(String salary) {
            try {
                this.salary = Double.parseDouble(salary);
            } catch (NumberFormatException exception) {
                throw new InvalidInputException(String.format(INVALID_SALARY, salary));
            }
        }

        private int validateDepartmentIdString(String departmentId) {
            int deptId = 0;
            try {
                deptId = Integer.parseInt(departmentId);
            } catch (NumberFormatException exception) {
                throw new InvalidInputException(String.format(INVALID_DEPARTMENT, departmentId));
            }
            return deptId;
        }

        private boolean isNullOrEmpty(String data) {
            if (Objects.isNull(data) || data.isEmpty()) {
                return true;
            } else {
                return false;
            }
        }
    }
}
