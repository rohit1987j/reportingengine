package com.domain;

import com.exception.InvalidInputException;

import java.util.Objects;

public class Department {
    private final String name;
    private final int id;

    private Department(final String name, final int id) {
        this.name = name;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public static class Builder {
        private static final String DEPARTMENT_NAME_CANNOT_BE_NULL_OR_EMPTY = "Department name cannot be null or empty";
        private static final String DEPARTMENT_ID_CANNOT_LESS_THEN_ZERO = "Department id cannot be less then one";

        private String name;
        private int id;

        public Builder setId(final int id) {
            validateId(id);
            this.id = id;
            return this;
        }

        public Builder setName(final String name) {
            validateName(name);
            this.name = name;
            return this;
        }

        public Department build() {
            performValidations();
            return new Department(name, id);
        }

        private void performValidations() {
            validateName(this.name);
            validateId(this.id);
        }

        private void validateName(final String name) {
            if (isNullOrEmpty(name)) {
                throw new InvalidInputException(DEPARTMENT_NAME_CANNOT_BE_NULL_OR_EMPTY);
            }
        }

        private void validateId(final int id) {
            if (id < 1) {
                throw new InvalidInputException(DEPARTMENT_ID_CANNOT_LESS_THEN_ZERO);
            }
        }

        private boolean isNullOrEmpty(String data) {
            if (Objects.isNull(data) || data.isEmpty()) {
                return true;
            } else {
                return false;
            }
        }
    }
}
