package com.domain;

import com.exception.InvalidInputException;

import java.util.Objects;

public class Age {
    private final String employeeName;
    private final int age;

    private Age(String employeeName, int age) {
        this.employeeName = employeeName;
        this.age = age;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public int getAge() {
        return age;
    }

    public static final class Builder {
        private static final String INVALID_AGE = "Invalid Age : %s";
        private static final String EMPLOYEE_NAME_CANNOT_BE_NULL = "Employee name cannot be null";
        private static final String EMPLOYEE_AGE_CANNOT_BE_LESS_THEN_18 = "Employee age cannot be less then 18";

        private String employeeName;
        private int age;

        public Builder setName(final String employeeName) {
            this.employeeName = employeeName;
            return this;
        }

        public Builder setAge(final int age) {
            this.age = age;
            return this;
        }

        public Builder setAge(final String age) {
            this.age = validateIfAgeIsInt(age);
            return this;
        }

        public Age build() {
            performValidations();
            return new Age(employeeName, age);
        }

        private int validateIfAgeIsInt(final String age) {
            int ag = 0;
            try {
                ag = Integer.parseInt(age);
            } catch (NumberFormatException exception) {
                throw new InvalidInputException(String.format(INVALID_AGE, age));
            }
            return ag;
        }

        private void validateEmployeeName(final String employeeName) {
            if (isNullOrEmpty(employeeName)) {
                throw new InvalidInputException(EMPLOYEE_NAME_CANNOT_BE_NULL);
            }
        }

        private void validateEmployeeAge(final int age) {
            if (age < 18) {
                throw new InvalidInputException(EMPLOYEE_AGE_CANNOT_BE_LESS_THEN_18);
            }
        }

        private void performValidations() {
            validateEmployeeName(employeeName);
            validateEmployeeAge(age);
        }

        private boolean isNullOrEmpty(String data) {
            if (Objects.isNull(data) || data.isEmpty()) {
                return true;
            } else {
                return false;
            }
        }
    }

    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }
        if (object instanceof Age) {
            Age age = (Age) object;
            if (this.getEmployeeName().equals(age.getEmployeeName()) && this.getAge() == this.age) {
                return true;
            }
        }
        return false;
    }
}
