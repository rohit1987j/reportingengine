package com.reader;

import com.domain.Department;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class DepartmentCSVReader extends CSVReader {
    private static final String FILE_NAME = "departments.csv";

    private List<Department> departmentList;

    public List<Department> getDepartmentList() {
        return departmentList;
    }

    private final FileReaderService fileReaderService;

    public DepartmentCSVReader(FileReaderService fileReaderService) {
        this.fileReaderService = fileReaderService;
    }

    @Override
    public void initializeList(String filePath) throws IOException {
        departmentList = new ArrayList<>();
        final List<List<String>> list = fileReaderService.readFromFile(filePath + FILE_NAME);
        final List<String> dList = list.parallelStream().flatMap(Collection::stream).sorted().collect(Collectors.toList());
        int id = 0;
        for (String departmentName : dList) {
            departmentList.add(new Department.Builder().setId(++id).setName(departmentName).build());
        }
    }
}
