package com.reader;

import com.domain.Age;
import com.domain.Employee;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class EmployeeCSVReader extends CSVReader {

    private static final String FILE_NAME = "employees.csv";

    private List<Employee> employeeList;

    private List<Age> ageList;

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    private final FileReaderService fileReaderService;

    public EmployeeCSVReader(final List<Age> ageList, FileReaderService fileReaderService) {
        this.ageList = ageList;
        this.fileReaderService = fileReaderService;
    }

    @Override
    public void initializeList(String filePath) throws IOException {
        employeeList = new ArrayList<>();
        for (List<String> details : fileReaderService.readFromFile(filePath + FILE_NAME)) {
            final String employeeName = details.get(1);
            final int age = getAgeByEmployeeName(employeeName);
            employeeList.add(new Employee.Builder().setDepartmentId(details.get(0)).setName(
                    employeeName).setGender(details.get(2)).setSalary(
                    details.get(3)).setAge(age).build());
        }
    }

    private int getAgeByEmployeeName(final String employeeName) {
        Optional<Age> ageObject = ageList.stream().filter(a -> a.getEmployeeName().equals(employeeName)).findFirst();
        int age = 0;
        if (ageObject.isPresent()) {
            age = ageObject.get().getAge();
        }
        return age;
    }
}