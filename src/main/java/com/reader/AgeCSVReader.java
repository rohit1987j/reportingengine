package com.reader;

import com.domain.Age;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AgeCSVReader extends CSVReader {
    private static final String FILE_NAME = "ages.csv";

    private List<Age> ageList;

    private final FileReaderService fileReaderService;

    public AgeCSVReader(FileReaderService fileReaderService) {
        this.fileReaderService = fileReaderService;
    }

    public List<Age> getAgeList() {
        return ageList;
    }

    @Override
    public void initializeList(final String filePath) throws IOException {
        ageList = new ArrayList<>();
        for (List<String> details : fileReaderService.readFromFile(filePath + FILE_NAME)) {
            ageList.add(new Age.Builder().setName(details.get(0)).setAge(details.get(1)).build());
        }
    }
}
