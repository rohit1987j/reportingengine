package com.reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class FileWriterService {
    public void writeListToFile(final List<String> list, final String filePath) throws IOException {
        Files.write(Paths.get(filePath), list);
    }
}
