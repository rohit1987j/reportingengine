package com.reader;

import java.io.IOException;

public abstract class CSVReader {

    public abstract void initializeList(String directory) throws IOException;
}
