package com.reader;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FileReaderService {

    public List<List<String>> readFromFile(String filePath) throws IOException {
        Stream<String> lines = Files.lines(Paths.get(filePath));
        return lines.map(line -> Arrays.asList(line.split(","))).collect(Collectors.toList());
    }
}
