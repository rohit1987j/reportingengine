package com;

import com.domain.Age;
import com.domain.Department;
import com.domain.Employee;
import com.reader.AgeCSVReader;
import com.reader.DepartmentCSVReader;
import com.reader.EmployeeCSVReader;
import com.reader.FileReaderService;

import java.io.IOException;
import java.util.List;

public class Configuration {
    private List<Employee> employeeList;
    private List<Department> departmentList;
    private List<Age> ageList;

    public void initialize(String path) throws IOException {

        initializeDepartmentList(path);

        initializeAgeList(path);

        initializeEmployeeList(path);

    }

    private void initializeDepartmentList(String path) throws IOException {
        DepartmentCSVReader departmentCSVReader = new DepartmentCSVReader(new FileReaderService());
        departmentCSVReader.initializeList(path);
        departmentList = departmentCSVReader.getDepartmentList();
    }

    private void initializeAgeList(String path) throws IOException {
        AgeCSVReader ageCSVReader = new AgeCSVReader(new FileReaderService());
        ageCSVReader.initializeList(path);
        ageList = ageCSVReader.getAgeList();
    }

    private void initializeEmployeeList(String path) throws IOException {
        EmployeeCSVReader employeeCSVReader = new EmployeeCSVReader(ageList, new FileReaderService());
        employeeCSVReader.initializeList(path);
        employeeList = employeeCSVReader.getEmployeeList();
    }

    public List<Employee> getEmployeeList() {
        return employeeList;
    }

    public List<Department> getDepartmentList() {
        return departmentList;
    }

    public List<Age> getAgeList() {
        return ageList;
    }
}
