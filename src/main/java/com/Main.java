package com;

import com.exception.InvalidInputException;
import com.reader.FileWriterService;
import com.writer.ReportingService;

import java.io.IOException;
import java.util.Objects;

public class Main {

    private static final String PROVIDE_DIRECTORY_PATH = "Please provide path as argument";
    private static final String DIRECTORY_PATH_SHOULD_END_WITH_FILE_SEPRATOR =
            "Path should end with %s";
    private static final String FILE_SEPARATOR = "file.separator";

    private static Configuration configuration;

    public static void main(String[] args) throws IOException {
        validatePath(args);

        initializeApplication(args[0]);

        generateReports(args[0]);
    }

    private static void initializeApplication(String path) throws IOException {
        configuration = new Configuration();
        configuration.initialize(path);
    }

    private static void generateReports(String path) throws IOException {
        new ReportingService(new FileWriterService(), path).generateReports(
                configuration.getEmployeeList(), configuration.getDepartmentList());

        System.out.println("Task successfull- All reports generated");
    }

    private static void validatePath(String[] args) {
        if (Objects.isNull(args) || args.length != 1) {
            throw new InvalidInputException(PROVIDE_DIRECTORY_PATH);
        }

        if (!args[0].endsWith(System.getProperty(FILE_SEPARATOR))) {
            throw new InvalidInputException(
                    String.format(DIRECTORY_PATH_SHOULD_END_WITH_FILE_SEPRATOR,
                            System.getProperty(FILE_SEPARATOR)));
        }
    }
}