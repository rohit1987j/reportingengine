package com.writer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public abstract class ReportGenerator {
    protected static final String STRING_AND_STRING = "%s,%s";
    protected static final String DEPARTMENT_WITH_SALARY =
            String.format("DepartmentName, Salary %s", System.getProperty("line.separator"));

    protected final List<String> outputList;

    public List<String> getOutputList() {
        return outputList;
    }

    public ReportGenerator() {
        this.outputList = new ArrayList<>();
    }

    protected abstract void generate() throws IOException;
}