package com.writer;

import com.domain.Department;
import com.domain.Employee;
import com.reader.FileWriterService;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class GenerateIncome95ByDepartmentReport extends GenerateIncomeByDepartmentReport {
    private static final String FILE_NAME = "income-95-by-department.csv";

    public GenerateIncome95ByDepartmentReport(final FileWriterService fileWriterService, final List<Employee> employeeList, final List<Department> departmentList, final String path) {
        super(fileWriterService, employeeList, departmentList, path);
    }

    @Override
    public void generate() throws IOException {
        final Map<Integer, Double> map = getDepartmentIdWithAverageSalaryMap();

        final List<String> list = new ArrayList<>();
        list.add(DEPARTMENT_WITH_SALARY);

        final int departmentId = getDepartmentIdWith95Percentile(map);
        final String departmentName = getDepartmentNameWithId(departmentId);

        list.add(String.format(STRING_AND_STRING, departmentName, String.valueOf(map.get(departmentId))));

        fileWriterService.writeListToFile(list, path + FILE_NAME);
    }

    private String getDepartmentNameWithId(final int id) {
        String departmentName = null;
        for (Department department : departmentList) {
            if (department.getId() == id) {
                departmentName = department.getName();
                break;
            }
        }
        return departmentName;
    }

    private int getDepartmentIdWith95Percentile(final Map<Integer, Double> map) {
        final Map<Integer, Double> sortedMapBasedOnSalary = getSortedMapBasedOnSalary(map);

        int index = findIndexWithPercentile(sortedMapBasedOnSalary, 95);
        index--;

        int counter = 0;
        int deptId = -1;
        for (Integer key : sortedMapBasedOnSalary.keySet()) {
            if (counter == index) {
                deptId = key;
                break;
            }
            counter++;
        }
        return deptId;
    }

    private HashMap<Integer, Double> getSortedMapBasedOnSalary(final Map<Integer, Double> map) {
        return map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }

    private int findIndexWithPercentile(final Map<Integer, Double> sortedMapBasedOnSalary, final double percentile) {
        return (int) Math.ceil((percentile / (double) 100) * (double) sortedMapBasedOnSalary.size());
    }
}