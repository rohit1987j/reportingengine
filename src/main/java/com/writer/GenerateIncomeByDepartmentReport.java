package com.writer;

import com.domain.Department;
import com.domain.Employee;
import com.reader.FileWriterService;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GenerateIncomeByDepartmentReport extends ReportGenerator {
    private static final String FILE_NAME = "income-by-department.csv";

    protected final List<Employee> employeeList;
    protected final List<Department> departmentList;
    protected final FileWriterService fileWriterService;
    protected final String path;

    public GenerateIncomeByDepartmentReport(final FileWriterService fileWriterService,
                                            final List<Employee> employeeList, final List<Department> departmentList,
                                            final String path) {
        this.fileWriterService = fileWriterService;
        this.employeeList = employeeList;
        this.departmentList = departmentList;
        this.path = path;
    }

    protected Map<Integer, Double> getDepartmentIdWithAverageSalaryMap() {
        return employeeList.parallelStream().collect(Collectors.groupingBy(
                Employee::getDepartmentId,
                Collectors.averagingDouble(Employee::getSalary)));
    }

    @Override
    public void generate() throws IOException {
        final Map<Integer, Double> map = getDepartmentIdWithAverageSalaryMap();

        outputList.add(DEPARTMENT_WITH_SALARY);

        addDataToList(map, outputList);

        fileWriterService.writeListToFile(outputList, path + FILE_NAME);
    }

    private void addDataToList(final Map<Integer, Double> map, final List<String> list) {
        for (Department department : departmentList) {
            final double averageSalary = map.get(department.getId());
            final String data = String.format(STRING_AND_STRING, department.getName(), String.valueOf(averageSalary));
            list.add(data);
        }
    }
}