package com.writer;

import com.domain.Department;
import com.domain.Employee;
import com.reader.FileWriterService;

import java.io.IOException;
import java.util.List;

public class ReportingService {
    private final FileWriterService fileWriterService;
    private final String path;

    public ReportingService(final FileWriterService fileWriterService, final String path) {
        this.fileWriterService = fileWriterService;
        this.path = path;
    }

    public void generateReports(List<Employee> employeeList, List<Department> departmentList) throws IOException {
        new GenerateEmployeeAgeByDepartmentReport(fileWriterService, departmentList, employeeList, path).generate();
        new GenerateIncome95ByDepartmentReport(fileWriterService, employeeList, departmentList, path).generate();
        new GenerateIncomeByDepartmentReport(fileWriterService, employeeList, departmentList, path).generate();
    }
}
