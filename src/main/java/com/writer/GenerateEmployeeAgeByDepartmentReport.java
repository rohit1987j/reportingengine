package com.writer;

import com.domain.Department;
import com.domain.Employee;
import com.reader.FileWriterService;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GenerateEmployeeAgeByDepartmentReport extends ReportGenerator {
    private static final String FILE_NAME = "employee-age-by-department.csv";

    private final String DEPARTMENT_WITH_AVERAGE_SALARY = "Department name,Average age";

    protected final List<Department> departmentList;
    protected final List<Employee> employeeList;
    protected final FileWriterService fileWriterService;
    private final String path;

    public GenerateEmployeeAgeByDepartmentReport(final FileWriterService fileWriterService,
                                                 final List<Department> departmentList, final List<Employee> employeeList,
                                                 final String path) {
        this.fileWriterService = fileWriterService;
        this.departmentList = departmentList;
        this.employeeList = employeeList;
        this.path = path;
    }

    @Override
    public void generate() throws IOException {
        final Map<Integer, Double> map = getDepartmentIdWithAverageAgeMap();

        outputList.add(DEPARTMENT_WITH_AVERAGE_SALARY);

        getAverageEmployeeAgeByDepartmentList(map, outputList);

        fileWriterService.writeListToFile(outputList, path+FILE_NAME);
    }

    private Map<Integer, Double> getDepartmentIdWithAverageAgeMap() {
        return employeeList.stream().collect(Collectors.groupingBy(
                Employee::getDepartmentId, Collectors.averagingInt(Employee::getAge)));
    }

    private void getAverageEmployeeAgeByDepartmentList(final Map<Integer, Double> map, final List<String> list) {
        for (Department department : departmentList) {

            final double averageAge = map.get(department.getId());

            final String data = String.format(STRING_AND_STRING, department.getName(), String.valueOf(averageAge));

            list.add(data);
        }
    }
}
