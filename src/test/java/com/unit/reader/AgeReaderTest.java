package com.unit.reader;

import com.domain.Age;
import com.reader.AgeCSVReader;
import com.reader.FileReaderService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AgeReaderTest {

    private AgeCSVReader ageCSVReader;

    @Mock
    private FileReaderService fileReaderService;

    @Before
    public void init() {
        ageCSVReader = new AgeCSVReader(fileReaderService);
    }

    @Test
    public void shouldInitializeAgeListWhenFilePathIsCorrect() throws IOException {
        final String path = "/randompath/randomfile.csv";

        List<List<String>> csvList = new ArrayList<>();
        for (int i = 0; i < 40; i++) {
            List<String> ageList = new ArrayList<>();
            ageList.add("Dummy Employee " + i);
            ageList.add(String.valueOf(i + 20));
            csvList.add(ageList);
        }

        when(fileReaderService.readFromFile(ArgumentMatchers.anyString())).thenReturn(csvList);

        ageCSVReader.initializeList(path);
        for (int i = 0; i < 40; i++) {
            assertEquals(ageCSVReader.getAgeList().get(i), new Age.Builder().setName("Dummy Employee " + i).setAge(i + 20).build());
        }
    }
}
