package com.unit.domain;

import com.domain.Department;
import com.exception.InvalidInputException;
import org.junit.Test;

public class DepartmentTest {

    @Test(expected = InvalidInputException.class)
    public void shouldThrowInvalidInputExceptionWhenNameIsNull() throws InvalidInputException {
        new Department.Builder().setId(1).build();
    }

    @Test(expected = InvalidInputException.class)
    public void shouldThrowInvalidInputExceptionWhenNameIsEmpty() throws InvalidInputException {
        new Department.Builder().setName("").build();
    }

    @Test(expected = InvalidInputException.class)
    public void shouldThrowInvalidInputExceptionWhenIdIsLessThen1() throws InvalidInputException {
        new Department.Builder().setId(0).build();
    }

    @Test
    public void shouldWorkFineWhenNameAndIdAreValid() throws InvalidInputException {
        new Department.Builder().setName("IT Department").setId(1).build();
    }
}
