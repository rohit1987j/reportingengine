package com.unit.domain;

import com.domain.Employee;
import com.exception.InvalidInputException;
import org.junit.Test;

public class EmployeeTest {
    @Test(expected = InvalidInputException.class)
    public void shouldThrowInvalidInputExceptionWhenAgeIsNotSet() throws InvalidInputException {
        new Employee.Builder().setName("DummyEmployee").setSalary(1).setGender("M").setDepartmentId(1).build();
    }

    @Test(expected = InvalidInputException.class)
    public void shouldThrowInvalidInputExceptionWhenNameIsEmpty() throws InvalidInputException {
        new Employee.Builder().setName("").build();
    }

    @Test(expected = InvalidInputException.class)
    public void shouldThrowInvalidInputExceptionWhenNameIsNull() throws InvalidInputException {
        new Employee.Builder().setName(null).setSalary(1).setGender("M").setAge(1).setDepartmentId(1).build();
    }

    @Test(expected = InvalidInputException.class)
    public void shouldThrowInvalidInputExceptionWhenDepartmentIdIsNotSet() throws InvalidInputException {
        new Employee.Builder().setName("Dummy employee").setSalary(1).setGender("M").setAge(1).build();
    }

    @Test(expected = InvalidInputException.class)
    public void shouldThrowInvalidInputExceptionWhenGenderIsNull() throws InvalidInputException {
        new Employee.Builder().setName("Dummy employee").setSalary(1).setGender(null).setAge(1).setDepartmentId(1).build();
    }

    @Test(expected = InvalidInputException.class)
    public void shouldThrowInvalidInputExceptionWhenGenderIsEmpty() throws InvalidInputException {
        new Employee.Builder().setName("Dummy employee").setSalary(1).setGender("").setAge(1).setDepartmentId(1).build();
    }

    @Test
    public void shouldWorkFineWhenEveryAttributeIsValid() throws InvalidInputException {
        new Employee.Builder().setName("Dummy employee").setGender("M").setSalary(1).setAge(18).setDepartmentId(1).build();
    }
}
