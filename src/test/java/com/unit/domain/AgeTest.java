package com.unit.domain;

import com.domain.Age;
import com.exception.InvalidInputException;
import org.junit.Test;

public class AgeTest {

    @Test(expected = InvalidInputException.class)
    public void ageBuildShouldThrowInvalidInputExceptionWhenAgeIsNull() throws InvalidInputException {
        new Age.Builder().setName("DummyEmployee").build();
    }

    @Test(expected = InvalidInputException.class)
    public void ageBuildShouldThrowInvalidInputExceptionWhenNameIsNull() throws InvalidInputException {
        new Age.Builder().setAge(19).build();
    }

    @Test(expected = InvalidInputException.class)
    public void ageBuildShouldThrowInvalidInputExceptionWhenAgeIsNotValid() throws InvalidInputException {
        new Age.Builder().setAge("Age Cannot be String").build();
    }

    @Test
    public void ageBuildShouldWorkFineWhenAgeAndNameIsValid() throws InvalidInputException {
        new Age.Builder().setAge("19").setName("DummyEmployee").build();
    }
}
