package com.unit.writer;


import com.domain.Department;
import com.domain.Employee;
import com.exception.InvalidInputException;
import com.reader.FileWriterService;
import com.writer.GenerateIncomeByDepartmentReport;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class GenerateIncomeByDepartmentReportTest {
    @Mock
    private FileWriterService fileWriterService;

    private GenerateIncomeByDepartmentReport generateIncomeByDepartmentReport;

    @Spy
    private List<Employee> employeeList;

    @Spy
    private List<Department> departmentList;

    @Before
    public void init() {
        departmentList = new ArrayList<>();
        employeeList = new ArrayList<>();
        generateIncomeByDepartmentReport = new GenerateIncomeByDepartmentReport(fileWriterService, employeeList, departmentList, "");
    }

    @Test
    public void generateShouldCreateList() throws InvalidInputException, IOException {

        for (int i = 1; i <= 5; i++) {
            departmentList.add(new Department.Builder().setName("department" + i).setId(i).build());
        }

        Map<String, Double> map = new LinkedHashMap<>();

        for (int i = 1; i <= 5; i++) {
            employeeList.add(new Employee.Builder().setName("male dummy employee" + i).
                    setDepartmentId(i).setAge(i * 18).setSalary(i * 500).setGender("M").build());

            employeeList.add(new Employee.Builder().setName("female dummy employee" + i).
                    setDepartmentId(i).setAge(i * 18).setSalary(i * 600).setGender("F").build());
            map.put(departmentList.get(i -1).getName(), (i * 500.0 + i * 600)/2);
        }

        generateIncomeByDepartmentReport.generate();

        final List<String> outputList = new ArrayList<>();
        outputList.add(String.format("DepartmentName, Salary"));
        for (String departmentName: map.keySet()) {
            outputList.add(String.format("%s,%s", departmentName, map.get(departmentName)));
        }

        assertEquals(outputList.size(), generateIncomeByDepartmentReport.getOutputList().size());

        for(int counter = 0; counter < outputList.size(); counter++) {
            assertEquals(outputList.get(counter).trim(), generateIncomeByDepartmentReport.getOutputList().get(counter).trim());
        }
    }
}